<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;

class tokopediaController extends Controller
{
    public function orderTokped() {
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        $token = json_decode($res);
        
        // tess product
        // $clientProduct = new Client();

        // $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/etalase?shop_id=10696883", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        
        // $productEtalase = json_decode($resProduct);
        // dd($productEtalase);


        // tokped order api
        $current_time = Carbon::now()->timestamp;
        // 30 menit yang lalu
        $start_time = $current_time - 1800;
        
        $tokpedOrders = [];
        $shopsData = [
            'warisan Sriwijaya' => [
                'shopsId' => 10696883,
                'to' => "https://www.tokopedia.com/warisansriwijaya"
            ],
            'Warisan Gajahmada Yogyakarta' => [
                'shopsId' => 9235772,
                'to' => "https://www.tokopedia.com/Wgmyogya"
            ],
            'Warisan Gajahmada Denpasar' => [
                'shopsId' => 9583529,
                'to' => "https://www.tokopedia.com/Warisangajahmadadenpasar"
            ],
            'Warisan Gajahmada Cirebon' => [
                'shopsId' => 9585109,
                'to' => "https://www.tokopedia.com/Warisangajahmadacirebon"
            ],
            'Warisan Gajahmada Surabaya' => [
                'shopsId' => 9235772,
                'to' => "https://www.tokopedia.com/warisangajahmadasurabaya"
            ],
            
        ];
        foreach ($shopsData as $item => $key) {
            // dd($key);
            $id_number = $key['shopsId'];
            $clientOrder = new Client();
            $resOrder = $clientOrder->get("https://fs.tokopedia.net/v2/order/list?fs_id=$appID&shop_id=$id_number&from_date=$start_time&to_date=$current_time&page=1&per_page=5&status=200", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $orders = json_decode($resOrder);
            if($orders->data != null) {
                DB::table('order_from_marketplace')->insert(['orderId' => $orders->data->order_id,  'platform' => 'Tokopedia', 'nmToko' => $orders->data->shop_id, 'created_at' => date('Y-m-d H:i:s')]);
                // array_push($tokpedOrders, $orders->data->shop_id);
                $config['content'] = "Order Masuk di Tokopedia";
                $config['to'] = $key['to'];
                $config['id_cms_users'] = [1];
                CRUDBooster::sendNotification($config);  
            }
            if($orders->data == null) {
                array_push($tokpedOrders, $orders->header->messages);
            }
        }
        // dd($orders);
        
        dd($tokpedOrders);
        
        // \DB::table('tes_data')->insert(["data_1" => '1']);
    }
}
