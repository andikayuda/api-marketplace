<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use function GuzzleHttp\json_decode;

class blibliController extends Controller
{
    //
    public function getOrder(){
        $sellerCode = [];
        // $sellerCode[0]['sellerCode'] = 'WAM-70000';
        // $sellerCode[0]['nmToko'] = 'Warisan Majapahit';
        // $sellerCode[0]['sellerApiKey'] = 'C9F7F44E8E5BD1B40E96456DD23F45A792E3EEAE60F12C7A73030ACBDB3A3DA7';
        // $sellerCode[1]['sellerCode'] = 'WAG-70003';
        // $sellerCode[1]['nmToko'] = 'Warisan Gajahmada JakUt';
        // $sellerCode[1]['sellerApiKey'] = ' ';
        // $sellerCode[2]['sellerCode'] = 'WAG-70004';
        // $sellerCode[2]['nmToko'] = 'Warisan Gajahmada JakSel';
        // $sellerCode[2]['sellerApiKey'] = ' ';
        // $sellerCode[3]['sellerCode'] = 'WAG-70012';
        // $sellerCode[3]['nmToko'] = 'Warisan Gajahmada Depok';
        // $sellerCode[3]['sellerApiKey'] = ' ';
        // $sellerCode[4]['sellerCode'] = 'WAG-70010';
        // $sellerCode[4]['nmToko'] = 'Warisan Gajahmada Bekasi';
        // $sellerCode[4]['sellerApiKey'] = ' ';
        // $sellerCode[5]['sellerCode'] = 'WAG-70013';
        // $sellerCode[5]['nmToko'] = 'Warisan Gajahmada Bogor';
        // $sellerCode[5]['sellerApiKey'] = ' ';
        // $sellerCode[6]['sellerCode'] = 'WAG-70011';
        // $sellerCode[6]['nmToko'] = 'Warisan Gajahmada Jakbar';
        // $sellerCode[6]['sellerApiKey'] = ' ';
        // $sellerCode[7]['sellerCode'] = 'WAG-70030';
        // $sellerCode[7]['nmToko'] = 'Warisan Gajahmada Surabaya';
        // $sellerCode[7]['sellerApiKey'] = '93B86D47253DA2D08013EE89BF3D1605E3F2797E7CCBC9CF039A198358CB1E7F';
        // $sellerCode[8]['sellerCode'] = 'WAG-70024';
        // $sellerCode[8]['nmToko'] = 'Warisan Gajahmada Cirebon';
        // $sellerCode[8]['sellerApiKey'] = '06A825311F5952C887427318A3E0AE60101B7F2FA76D3313B0D10E2A655F38ED';
        // $sellerCode[9]['sellerCode'] = 'WAG-70023';
        // $sellerCode[9]['nmToko'] = 'Warisan Gajahmada Jogjakarta';
        // $sellerCode[9]['sellerApiKey'] = '280E783B67C9B1667A3962C4D16FA729F65FE6ADBB455E971B75C56D450B2B65';
        // $sellerCode[10]['sellerCode'] = 'WAG-70021';
        // $sellerCode[10]['nmToko'] = 'Warisan Gajahmada Bali';
        // $sellerCode[10]['sellerApiKey'] = '1DD1633C4262CA0D78735132000ACBD93CDB90AE4DCDDA2DCB90339B1BDD268C';
        // $sellerCode[11]['sellerCode'] = 'WAG-60046';
        // $sellerCode[11]['nmToko'] = 'Warisan Gajahmada';
        // $sellerCode[11]['sellerApiKey'] = '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1';
        
        $sellerCode[0]['sellerCode'] = 'WAM-70000';
        $sellerCode[0]['nmToko'] = 'Warisan Majapahit';
        $sellerCode[0]['sellerApiKey'] = 'C9F7F44E8E5BD1B40E96456DD23F45A792E3EEAE60F12C7A73030ACBDB3A3DA7';
        $sellerCode[1]['sellerCode'] = 'WAG-70030';
        $sellerCode[1]['nmToko'] = 'Warisan Gajahmada Surabaya';
        $sellerCode[1]['sellerApiKey'] = '93B86D47253DA2D08013EE89BF3D1605E3F2797E7CCBC9CF039A198358CB1E7F';
        // $sellerCode[2]['sellerCode'] = 'WAG-70024';
        // $sellerCode[2]['nmToko'] = 'Warisan Gajahmada Cirebon';
        // $sellerCode[2]['sellerApiKey'] = '06A825311F5952C887427318A3E0AE60101B7F2FA76D3313B0D10E2A655F38ED';
        $sellerCode[2]['sellerCode'] = 'WAG-70023';
        $sellerCode[2]['nmToko'] = 'Warisan Gajahmada Jogjakarta';
        $sellerCode[2]['sellerApiKey'] = '280E783B67C9B1667A3962C4D16FA729F65FE6ADBB455E971B75C56D450B2B65';
        $sellerCode[3]['sellerCode'] = 'WAG-70021';
        $sellerCode[3]['nmToko'] = 'Warisan Gajahmada Bali';
        $sellerCode[3]['sellerApiKey'] = '1DD1633C4262CA0D78735132000ACBD93CDB90AE4DCDDA2DCB90339B1BDD268C';
        $sellerCode[4]['sellerCode'] = 'WAG-60046';
        $sellerCode[4]['nmToko'] = 'Warisan Gajahmada';
        $sellerCode[4]['sellerApiKey'] = '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1';
        // dd($sellerCode);
        // $dataSeller = json_decode($sellerCode);
        // return $sellerCode;
        // $data = $sellerCode[0]['sellerCode'];
        // dd($data);
        $blibliOrders = [];
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");
        foreach($sellerCode as $seller){
            $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderList?status=FP&requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$seller['sellerCode'].'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $seller['sellerApiKey']
                ]])->getBody();
            $res = $res->getContents();
            $response = json_decode($res);
            $datas = $response->content;
            // dd($response->content);
            // dd($response);
            if($response->content != null) {
                foreach($datas as $data){
                    // dd($seller['sellerCode']);
                    $insert = DB::table('order_from_marketplace')->insert([
                        'orderId' => $data->orderNo, 
                        'platform' => 'Blibli',
                        'nmToko' => $seller['sellerCode'],
                        'created_at' => date('Y-m-d H:i:s')]);
                    $config['content'] = "Order Masuk di Blibli";
                    $config['to'] = $seller['nmToko'];
                    $config['id_cms_users'] = [1];
                    CRUDBooster::sendNotification($config);
                    array_push($blibliOrders, $response->content);   
                }
            }
        }
        
        // $data = json_decode($res);
        
        // $response = $response['content'];
        
        // $data = $data->content;
        // $data = $data->toJson();
        // dd($blibliOrders);
    }
}
